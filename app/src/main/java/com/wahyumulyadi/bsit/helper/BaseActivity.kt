package com.wahyumulyadi.bsit.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

abstract class BaseActivityy<B: ViewBinding> : AppCompatActivity() {
    protected lateinit var binding:B

    abstract fun initViewBinding():B
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = initViewBinding()
        setContentView(binding.root)
        onBindCreate()
    }

    abstract  fun onBindCreate()
}