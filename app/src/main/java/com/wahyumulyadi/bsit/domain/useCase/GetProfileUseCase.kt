package com.wahyumulyadi.bsit.domain.useCase

import com.wahyumulyadi.bsit.domain.repository.Repository
import com.wahyumulyadi.bsit.model.ProfileResponse
import com.wahyumulyadi.bsit.model.TransactionResponse
import retrofit2.Response
import javax.inject.Inject

class GetProfileUseCase @Inject constructor(
    private val repository: Repository
) {
    suspend fun getProfile():Response<ProfileResponse>{
        return repository.getProfile()
    }
}