package com.wahyumulyadi.bsit.domain.useCase

import com.wahyumulyadi.bsit.domain.repository.Repository
import com.wahyumulyadi.bsit.model.ContactResponse
import com.wahyumulyadi.bsit.model.TransactionResponse
import retrofit2.Response
import javax.inject.Inject

class GetContactUseCase @Inject constructor(
    private val repository: Repository
) {
    suspend fun getContact():Response<List<ContactResponse>>{
        return repository.getContact()
    }
}