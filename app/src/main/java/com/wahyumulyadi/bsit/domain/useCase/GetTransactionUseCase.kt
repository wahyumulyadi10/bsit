package com.wahyumulyadi.bsit.domain.useCase

import com.wahyumulyadi.bsit.domain.repository.Repository
import com.wahyumulyadi.bsit.model.TransactionResponse
import retrofit2.Response
import javax.inject.Inject

class GetTransactionUseCase @Inject constructor(
    private val repository: Repository
) {
    suspend fun getTransaction():Response<List<TransactionResponse>>{
        return repository.getTransaction()
    }
}