package com.wahyumulyadi.bsit.domain

import com.wahyumulyadi.bsit.model.ContactResponse
import com.wahyumulyadi.bsit.model.ProfileResponse
import com.wahyumulyadi.bsit.model.TransactionResponse
import retrofit2.Response

interface RemoteDataSource {
    suspend fun getTransaction():Response<List<TransactionResponse>>
    suspend fun getContact():Response<List<ContactResponse>>
    suspend fun getProfile():Response<ProfileResponse>
}