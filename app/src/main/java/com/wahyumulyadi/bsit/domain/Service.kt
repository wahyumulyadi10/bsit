package com.wahyumulyadi.bsit.domain

import com.wahyumulyadi.bsit.model.ContactResponse
import com.wahyumulyadi.bsit.model.ProfileResponse
import com.wahyumulyadi.bsit.model.TransactionResponse
import retrofit2.Response
import retrofit2.http.GET

interface Service {
    /**
        url
        https://private-54eacf-fazztrack.apiary-mock.com/questions
    **/
    @GET("profile")
    suspend fun getProfile():Response<ProfileResponse>

    @GET("contact")
    suspend fun  getContact():Response<List<ContactResponse>>


    @GET("transaction")
    suspend fun getTransaction():Response<List<TransactionResponse>>
}