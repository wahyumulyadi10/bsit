package com.wahyumulyadi.bsit.presentation.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.wahyumulyadi.bsit.databinding.FragmentContactBinding
import com.wahyumulyadi.bsit.model.ContactResponse
import com.wahyumulyadi.bsit.presentation.adapter.ContactAdapter
import com.wahyumulyadi.bsit.presentation.fragment.viewmodel.ContactViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ContactFragment : Fragment() {

    private var _binding: FragmentContactBinding? = null
    private val binding get() = _binding!!
    private  var contactAdapter= ContactAdapter()
    private val viewModel: ContactViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentContactBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewModel.getContact()
        binding.etSearch.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(query: Editable?) {
                val etValue = binding.etSearch.text

                if (etValue.isEmpty()) {
                    binding.ivClear.visibility = View.INVISIBLE
                }
                else {
                    binding.ivClear.visibility = View.VISIBLE

                }
                viewModel.searchContact(query.toString())
            }
        })

        binding.ivClear.setOnClickListener {
            binding.etSearch.text.clear()
        }

        //        binding?.rvContact.
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun observeViewModel() {
        viewModel.contact.observe(viewLifecycleOwner) {contactData->
            if (contactData != null){
                setData(contactData)
            }

        }
    }

    private fun  setData(data: List<ContactResponse>){
        contactAdapter.setData(data.toMutableList())

        binding?.apply {
            rvContact.adapter=contactAdapter

        }
        contactAdapter.onClickCall {
            callAFriend(it.noTelp.toString())
        }

    }
    private  fun callAFriend(phoneNumber:String){
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phoneNumber")
        startActivity(intent)
    }
}