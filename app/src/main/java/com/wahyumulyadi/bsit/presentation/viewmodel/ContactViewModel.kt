package com.wahyumulyadi.bsit.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wahyumulyadi.bsit.domain.useCase.GetContactUseCase
import com.wahyumulyadi.bsit.domain.useCase.GetTransactionUseCase
import com.wahyumulyadi.bsit.model.TransactionResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class ContactViewModel @Inject constructor(
    private  val  getContactUseCase: GetContactUseCase
) : ViewModel() {
       /* private val _transaction = MutableLiveData<List<TransactionResponse>>()
        val  transaction : LiveData<List<TransactionResponse>> get() = _transaction

    private val _errorMessage = MutableLiveData<String>()
    val  errorMessage : LiveData<String> get() = _errorMessage

    private val _showLoad = MutableLiveData<Boolean>()
    val  showLoad : LiveData<Boolean> get() = _showLoad

    fun getTransaction()= viewModelScope.launch {
            _showLoad.postValue(true)
            getTransactionUseCase.getTransaction().let{
                if (it.isSuccessful){
                    _transaction.postValue(it.body())
                    _showLoad.postValue(false)
                }else{
                    _showLoad.postValue(false)
                    _errorMessage.postValue(it.message())
                }
            }
        }*/
}