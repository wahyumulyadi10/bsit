package com.wahyumulyadi.bsit.presentation.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.wahyumulyadi.bsit.databinding.FragmentProfileBinding
import com.wahyumulyadi.bsit.model.ProfileResponse
import com.wahyumulyadi.bsit.presentation.fragment.viewmodel.ProfileViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding

    private val profileViewModel: ProfileViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding?.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        profileViewModel.getProfile()


    }

    private  fun seeLocation(lat :String, lng:String) {
        val uriParse = Uri.parse("google.navigation:q=$lat,$lng")
        val uri = Uri.parse("https://www.google.com/maps/search/?api=1&query=$lat,%20$lng")
        val intent = Intent(Intent.ACTION_VIEW, uriParse)
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent)
    }

    private fun initViewProfile(data: ProfileResponse) {
        binding?.apply {
            tvName.text = data.name
            tvJoinDate.text = data.joinedDate
            tvPhone.text = data.noTelp

            ivGoTolocation.setOnClickListener {
                seeLocation(data.lat.toString(),data.lng.toString())
            }
            if (data.status == 1) tvStatus.text = "Available" else tvStatus.text = "Not Available"
            Glide.with(root.context).load(data.imageUrl).circleCrop().into(ivProfile)
        }
    }

    private fun observeViewModel() {
        profileViewModel.profile.observe(viewLifecycleOwner) {
            initViewProfile(it)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}