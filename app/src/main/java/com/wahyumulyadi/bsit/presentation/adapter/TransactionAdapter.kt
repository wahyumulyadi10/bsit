package com.wahyumulyadi.bsit.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.wahyumulyadi.bsit.R
import com.wahyumulyadi.bsit.databinding.ItemTransactionBinding
import com.wahyumulyadi.bsit.model.TransactionResponse

class TransactionAdapter : RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {
    private val dataNew: MutableList<TransactionResponse> = mutableListOf()
    private var onClickList: (TransactionResponse) -> Unit = {}

    fun addDataNew(newData: List<TransactionResponse>) {
        dataNew.addAll(newData)
        notifyDataSetChanged()
    }


    inner class TransactionViewHolder(
        private val binding: ItemTransactionBinding
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(transaction: TransactionResponse) {
            binding.ccTvTitle.text = transaction.name
            binding.ccTvSubtitle.text = transaction.metodeTrf
            binding.ccTvPrice.text = transaction.nominalSaldo

            Glide.with(binding.root.context)
                .load(transaction.imageUrl)
                .circleCrop()
                .into(binding.ccImage)
            var flag = transaction.flagDebitCredit
            if (flag == 1)
                binding.ccTvPrice.setTextColor(binding.root.context.resources.getColor(R.color.red))
            else
                binding.ccTvPrice.setTextColor(binding.root.context.resources.getColor(R.color.green))
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val inflate = LayoutInflater.from(parent.context)
        val bind = ItemTransactionBinding.inflate(inflate, parent, false)
        return TransactionViewHolder(bind)
    }

    override fun getItemCount(): Int {
        return dataNew.size
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val item = dataNew[position]
        holder.bind(item)
    }
}