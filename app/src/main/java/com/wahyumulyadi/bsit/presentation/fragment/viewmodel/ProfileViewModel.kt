package com.wahyumulyadi.bsit.presentation.fragment.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wahyumulyadi.bsit.domain.useCase.GetContactUseCase
import com.wahyumulyadi.bsit.domain.useCase.GetProfileUseCase
import com.wahyumulyadi.bsit.domain.useCase.GetTransactionUseCase
import com.wahyumulyadi.bsit.model.ProfileResponse
import com.wahyumulyadi.bsit.model.TransactionResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val getProfileUseCase: GetProfileUseCase
):ViewModel() {
    private val _profile = MutableLiveData<ProfileResponse>()
    val profile: LiveData<ProfileResponse>get() = _profile

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> get() = _errorMessage

    private val _showLoad = MutableLiveData<Boolean>()
    val showLoad: LiveData<Boolean> get() = _showLoad

    fun getProfile() = viewModelScope.launch {
        _showLoad.postValue(true)
        getProfileUseCase.getProfile().let {
            if (it.isSuccessful) {
                _profile.postValue(it.body())
                _showLoad.postValue(false)
            } else {
                _showLoad.postValue(false)
                _errorMessage.postValue(it.message())
            }
        }
    }

}