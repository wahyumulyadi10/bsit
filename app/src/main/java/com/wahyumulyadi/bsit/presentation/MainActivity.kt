package com.wahyumulyadi.bsit.presentation

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayoutMediator
import com.wahyumulyadi.bsit.databinding.ActivityMainBinding
import com.wahyumulyadi.bsit.model.ContactResponse
import com.wahyumulyadi.bsit.model.ProfileResponse
import com.wahyumulyadi.bsit.model.TransactionResponse
import com.wahyumulyadi.bsit.presentation.adapter.BsitFragmentPagerAdapter
import com.wahyumulyadi.bsit.presentation.adapter.TransactionAdapter
import com.wahyumulyadi.bsit.presentation.fragment.ContactFragment
import com.wahyumulyadi.bsit.presentation.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val mainViewModel by viewModels<MainViewModel>()
/*
    private lateinit var viewPagerAdapter: ViewPagerAdapter*/

    private val transactionAdapter = TransactionAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpViewPager(listOf())
        setUpTabLayout()
        observeViewModel()
        mainViewModel.getTransaction()
//        mainViewModel.getContact()

    }

    private fun setUpViewPager(bundle: List<Bundle>) {
        val viewPager = binding.vpContainer
        viewPager.adapter = BsitFragmentPagerAdapter(this, bundle)
    }


    private fun setUpTabLayout() {
        val tabLayout = binding.tlFragment
        val viewPager = binding.vpContainer

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {

                0 -> tab.text = "Transaction"
                1 -> tab.text = "Contact"
                2 -> tab.text = "Profile"
            }
        }.attach()
    }



    private fun setDataForTransaction(data: List<TransactionResponse>) {
//        binding.rvListTransaction.adapter =transactionAdapter
//        binding.rvListTransaction.layoutManager = LinearLayoutManager(this)
        transactionAdapter.addDataNew(data)
    }

    private fun showLoad() {
        binding.cmpLoading.root.visibility = View.VISIBLE
//        binding.rvListTransaction.visibility =View.GONE
    }

    private fun hideLoad() {
        binding.cmpLoading.root.visibility = View.GONE
//        binding.rvListTransaction.visibility =View.VISIBLE
    }

//    private fun setData(
//        dataTransaction: List<TransactionResponse>,
//        dataContact: List<ContactResponse>,
//        dataProfile: ProfileResponse?
//    ) {
//        val bundle = listOf(
//            dataContact.let {
//                ContactFragment.createInstance(it)
//            }
//        )
//        setUpViewPager(bundle)
//    }

    private fun observeViewModel() {
        mainViewModel.transaction.observe(this) {
//            setData(it, listOf(), null)
            //   setDataForTransaction(it)
            //    Toast.makeText(applicationContext,it[0].name,Toast.LENGTH_LONG).show()
        }

       /* mainViewModel.contact.observe(this) {
            setData(listOf(), it, null)
               setDataForTransaction(it)
                Toast.makeText(applicationContext,it[0].name,Toast.LENGTH_LONG).show()
        }*/
       /* mainViewModel.profile.observe(this) {
            setData(listOf(), listOf(), it)
               setDataForTransaction(it)
                Toast.makeText(applicationContext,it[0].name,Toast.LENGTH_LONG).show()
        }*/
        mainViewModel.errorMessage.observe(this) {
            Toast.makeText(applicationContext, it, Toast.LENGTH_LONG).show()
        }
        mainViewModel.showLoad.observe(this) { isShow ->
            if (isShow) showLoad() else hideLoad()

        }

    }
}