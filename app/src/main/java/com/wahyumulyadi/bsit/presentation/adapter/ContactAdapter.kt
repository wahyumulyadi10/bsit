package com.wahyumulyadi.bsit.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.wahyumulyadi.bsit.databinding.ItemContactBinding
import com.wahyumulyadi.bsit.model.ContactResponse

class ContactAdapter: RecyclerView.Adapter<ContactAdapter.ContactViewHolder>(){


    private var data:MutableList<ContactResponse> = mutableListOf()
    private  var onClickPhone :(ContactResponse)->Unit ={}
    inner class ContactViewHolder(val binding: ItemContactBinding):RecyclerView.ViewHolder(
        binding.root

    ){
        fun bind(item: ContactResponse){
            binding.apply {


                tvName.text = item.name
                tvPhone.text = item.noTelp
                ivCall.setOnClickListener {
                    onClickPhone.invoke(item)
                }
            }
        }
    }
    fun onClickCall(click: (ContactResponse)->Unit){
        onClickPhone=click
    }

    fun setData(newData: MutableList<ContactResponse>){
        data=newData
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ContactViewHolder (
    ItemContactBinding.inflate(LayoutInflater.from(parent.context),parent,false)
    )

    override fun getItemCount(): Int {
        return  data.size
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.bind(data[position])
    }
}