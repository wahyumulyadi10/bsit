package com.wahyumulyadi.bsit.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wahyumulyadi.bsit.domain.useCase.GetContactUseCase
import com.wahyumulyadi.bsit.domain.useCase.GetTransactionUseCase
import com.wahyumulyadi.bsit.model.ContactResponse
import com.wahyumulyadi.bsit.model.ProfileResponse
import com.wahyumulyadi.bsit.model.TransactionResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val getTransactionUseCase: GetTransactionUseCase,
//    private val getContactUseCase: GetContactUseCase
) : ViewModel() {
    private val _transaction = MutableLiveData<List<TransactionResponse>>()
    val transaction: LiveData<List<TransactionResponse>> get() = _transaction
//
//    private val _contact = MutableLiveData<List<ContactResponse>>()
//    val contact: LiveData<List<ContactResponse>> get() = _contact
//
//    private val _profile = MutableLiveData<ProfileResponse>()
//    val profile: LiveData<ProfileResponse> get() = _profile


    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> get() = _errorMessage

    private val _showLoad = MutableLiveData<Boolean>()
    val showLoad: LiveData<Boolean> get() = _showLoad

    fun getTransaction() = viewModelScope.launch {
        _showLoad.postValue(true)
        getTransactionUseCase.getTransaction().let {
            if (it.isSuccessful) {
                _transaction.postValue(it.body())
                _showLoad.postValue(false)
            } else {
                _showLoad.postValue(false)
                _errorMessage.postValue(it.message())
            }
        }
    }
/*
    fun getContact() = viewModelScope.launch {
        _showLoad.postValue(true)
        getContactUseCase.getContact().let {
            if (it.isSuccessful) {
                _contact.postValue(it.body())
                _showLoad.postValue(false)
            } else {
                _showLoad.postValue(false)
                _errorMessage.postValue(it.message())
            }
        }
    }*/
}