package com.wahyumulyadi.bsit.presentation.fragment.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wahyumulyadi.bsit.domain.useCase.GetContactUseCase
import com.wahyumulyadi.bsit.model.ContactResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ContactViewModel @Inject constructor(
    private val getContactUseCase: GetContactUseCase
):ViewModel() {
    private val _contact = MutableLiveData<List<ContactResponse>>()
    val contact: LiveData<List<ContactResponse>> get() = _contact
    private  var dummyContact:MutableList<ContactResponse> = mutableListOf()
    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> get() = _errorMessage

    private val _showLoad = MutableLiveData<Boolean>()
    val showLoad: LiveData<Boolean> get() = _showLoad


    fun getContact() = viewModelScope.launch {
        _showLoad.postValue(true)
        getContactUseCase.getContact().let {
            if (it.isSuccessful) {
                dummyContact.addAll(it.body()?: mutableListOf())
                _contact.postValue(it.body())
                _showLoad.postValue(false)
            } else {
                _showLoad.postValue(false)
                _errorMessage.postValue(it.message())
            }
        }
    }
    fun searchContact(query:String){
        if (query.isEmpty()) {
            _contact.postValue(dummyContact)
        }
        else {

            val filterData = dummyContact.filter { dataContact ->
                dataContact.name?.contains(query, ignoreCase = true) ?: false
            }
            _contact.postValue(filterData)
        }

    }
}