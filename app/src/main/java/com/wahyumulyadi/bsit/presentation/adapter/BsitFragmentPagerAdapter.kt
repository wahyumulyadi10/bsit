package com.wahyumulyadi.bsit.presentation.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.wahyumulyadi.bsit.presentation.fragment.ContactFragment
import com.wahyumulyadi.bsit.presentation.fragment.ProfileFragment
import com.wahyumulyadi.bsit.presentation.fragment.TransactionFragment

class BsitFragmentPagerAdapter(
    fr:FragmentActivity,
    private  val bundle: List<Bundle>
    ):
    FragmentStateAdapter(fr) {

    private val fr = listOf(
        ::TransactionFragment,
        ::ContactFragment,
        ::ProfileFragment
    )


    override fun getItemCount(): Int {
        return  fr.size
    }

    override fun createFragment(position: Int): Fragment {
      val fragment = this.fr.getOrNull(position)
        val fragmentIntance= fragment?.invoke()  ?: Fragment()

        val bundle = bundle.getOrNull(position)
        fragmentIntance.arguments = bundle

        return fragmentIntance
    }
}